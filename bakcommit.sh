#!/bin/bash

LOGFILE=

function have_untracked_files() {
    git ls-files --other --exclude-standard --directory  # Empty (-z) means no untracked files
}

function have_unstaged_changes() {
    git diff --exit-code  # 0 means no changes
}

function have_uncommitted_changes() {
    git diff --cached --exit-code  # 0 means no changes
}

# Receives a path to a directory where local copies of the repos are stored
function get_repos_names() {
    find "$1" -maxdepth 1 -mindepth 1 -type d -printf '%f;'
}

# Stages all changes and commits them, and then pushes the results to remote
function autocommitpush() {
    cd "$1"
    
    if [ have_untracked_files ] || [ have_unstaged_changes ]
    then
        echo "Staging untracked files..." >> "$LOGFILE"
        git add -A >> "$LOGFILE"
    fi
    
    if [ have_uncommitted_changes ]
    then
        echo 'Committing changes...' >>"$LOGFILE" 2>&1
        git commit -m "DAILY BACKUP (`date`)" >>"$LOGFILE" 2>&1
        echo 'Pulling remote...' >>"$LOGFILE" 2>&1
        git pull >>"$LOGFILE" 2>&1
        echo 'Pushing new state to remote...' >>"$LOGFILE" 2>&1
        git push >>"$LOGFILE" 2>&1
    fi
    
    cd -
}

# ==================================================================================
# ==================================================================================
# ==================================================================================

D=`date +'%d%m%y_%H%M%S'`
LOGFILE="bakcommit.$D.log"
if ! [ -d "$HOME/.bakcommit" ]
then
    mkdir "$HOME/.bakcommit"
fi
touch "$HOME/.bakcommit/$LOGFILE"
LOGFILE="$HOME/.bakcommit/$LOGFILE"

echo "Autosaving repository changes under $1" >>"$LOGFILE" 2>&1
IFS=';' read -ra REPOS <<< $(get_repos_names "$1")
for r in "${REPOS[@]}"
do
    echo "Processing $1/$r..." >>"$LOGFILE" 2>&1
    autocommitpush "$1/$r" >>"$LOGFILE" 2>&1
    echo "Done with $1/$r" >>"$LOGFILE" 2>&1
    echo "---------------" >>"$LOGFILE" 2>&1
    echo '' >>"$LOGFILE" 2>&1
done

exit 0
